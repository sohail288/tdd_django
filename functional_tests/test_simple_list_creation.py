#import unittest
import time

from selenium import webdriver
from selenium.webdriver.common.keys import Keys

from .base import FunctionalTest


class NewVisitorTest(FunctionalTest):

    def test_can_start_a_list_and_retrieve_it_later(self):
    #he types in the address for his new favorite todo list
        self.browser.get(self.server_url)

    #joe sees that he's not on a porno site, but the site he expects by
    #looking at the title
        self.assertIn('To-Do', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('To-Do', header_text)

    #He sees that he can enter a todo item
        inputbox = self.browser.find_element_by_id('id_new_item')
        self.assertEqual(
        inputbox.get_attribute('placeholder'),
        'Enter a to-do item'
    )
    #He types in that he needs to get oil
        inputbox.send_keys('Buy oil')
        #He is taken to a new url when he hits enter
    #when he hits enter, the page shows the item entered
    # 1. buy oil
        inputbox.send_keys(Keys.RETURN)
        joe_list_url = self.browser.current_url
        self.assertRegexpMatches(joe_list_url, '/lists/.+')
        self.check_for_row_in_list_table('1: Buy oil')
        #self.assertTrue(
    #   any(row.text == '1: Buy oil' for row in rows),
    #   "New to-do item did not appear in table -- its text was:\n%s" % (
    #       table.text,
    #   )
    #)
        inputbox = self.browser.find_element_by_id('id_new_item')


    #he can add more items
    #he adds he also needs a oilpan
        inputbox.send_keys('Get oilpan')
        inputbox.send_keys(Keys.RETURN)

    #page updates
    #now both items are on the list
        self.check_for_row_in_list_table('1: Buy oil')
        self.check_for_row_in_list_table('2: Get oilpan')
        

        # A new user, Francis, come along to the site.
        #We use a new browser session to make sure tha tno info
        ## of edith's is coming through from cookies
        self.browser.quit()
        self.browser = webdriver.Firefox()

        #Francis visits the homepage. No sign of joes stuff
        self.browser.get(self.server_url)
        page_text = self.browser.find_element_by_tag_name('body').text
        self. assertNotIn('Buy oil', page_text)
        self.assertNotIn('Get oilpan', page_text)

        #francis starts a new list by entering a newitem.
        inputbox = self.browser.find_element_by_id('id_new_item')
        inputbox.send_keys('Buy milk')
        time.sleep(5)
        inputbox.send_keys(Keys.RETURN)
        
        #Francis gets his own url
        francis_list_url = self.browser.current_url
        self.assertRegexpMatches(francis_list_url, '/lists/.+')
        self.assertNotEqual(francis_list_url, joe_list_url)
        #none of joes stuff shows up
        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertNotIn('Buy oil', page_text)
        self.assertIn('Buy milk', page_text)
    #he visits the link
    #the list is there

    #joe goes off

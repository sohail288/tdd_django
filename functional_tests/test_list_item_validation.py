#import unittest
from unittest import skip


from selenium import webdriver
from selenium.webdriver.common.keys import Keys

from .base import FunctionalTest


class ItemValidationTest(FunctionalTest):


    def test_cannot_add_empty_list_items(self):
        # Joe goes onto the homepage and accidentally hits the submit button

        # An error notification pops up and says that he cant submit empty item

        # He submits some text this time and it works

        # joe, with twitchy fingers, submits another empty item

        # get's another error

        # And then fixes his mistake

        self.fail("Write me!")         



